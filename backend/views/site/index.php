<?php

/* @var $this yii\web\View
 * @var $userCount
 * @var $user \common\models\User
 */

$this->title = 'My Yii Application';
?>

<div class="site-index">

    <div class="jumbotron">
        <h1>Witamy w administracji strony Phobuk</h1>
        <p class="lead">Możesz zarządzać wszystkim :)</p>
        <h3>Ilość użytkowników: <?= $userCount ?></h3>
    </div>
</div>

