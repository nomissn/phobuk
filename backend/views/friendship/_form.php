<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Friendship */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="friendship-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'friend_one')->textInput() ?>

    <?= $form->field($model, 'friend_two')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
